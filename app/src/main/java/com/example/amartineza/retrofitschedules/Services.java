package com.example.amartineza.retrofitschedules;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Query;

/**
 * Created by amartineza on 3/27/2018.
 */

public interface Services {

    @Headers("api_key:es_cinepolis_test_android")
    @GET("v2/schedules")
    Call<SchedulesModel> getSchudels(@Query("country_code") String countryCode,
                                     @Query("cities") int cities,
                                     @Query("include_cinemas") boolean includeCinemas,
                                     @Query("include_movies") boolean includeMovies);
}
